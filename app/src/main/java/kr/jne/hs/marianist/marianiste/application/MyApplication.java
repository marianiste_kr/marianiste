package kr.jne.hs.marianist.marianiste.application;

import android.app.Application;

import kr.jne.hs.marianist.marianiste.util.Command;
import kr.jne.hs.marianist.marianiste.util.db.StorageHelper;

/**
 * Created by wins on 2017-06-05.
 */

public class MyApplication extends Application {
    public static Command COMMAND;
    public static StorageHelper SH;
    @Override
    public void onCreate() {
        super.onCreate();
        COMMAND = new Command(this);
        SH = StorageHelper.getInstance(this);
    }
}
