package kr.jne.hs.marianist.marianiste.util.enc;

import android.util.Base64;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import kr.jne.hs.marianist.marianiste.util.consts.E;

/**
 * Created by Penny on 2017-03-15.
 * 현재 나온 보안 방식 중 가장 안전한 보안 방식
 * key를 서버로 부터 전달 받아 key와 string 값을 혼합하여 새로운 값을 만든다.
 * 보안에 사용할 정보 : 이름, 전화번호(필요시), uid, 기타 계정 정보, 내부 데이터에 저장되는 값
 */

public class AES256Cipher {
    public static class AES {
        public static String Enc(final String strData) {
            return AES256Cipher.Encryption(strData);
        }

        public static String Desc(final String strData) {
            return AES256Cipher.Decryption(strData);
        }

    }

    private static String key = E.Net.SECURE_KEY;


    public static byte[] ivBytes = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};


    public static String Encryption(String Input) {
        try {
            return AES_Encode(Input, key);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    public static String Decryption(String Input) {
        try {
            return AES_Decode(Input, key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private static String AES_Encode(String str, String key) throws java.io.UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

        byte[] textBytes = str.getBytes("UTF-8");
        AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
        SecretKeySpec newKey = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
        Cipher cipher = null;
        cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, newKey, ivSpec);

        String Output = Base64.encodeToString(cipher.doFinal(textBytes), Base64.NO_WRAP);

        Output = Output.replace('+', '-');
        Output = Output.replace('/', '_');
        Output = Output.replace('=', '~');

        return Output;

    }

    private static String AES_Decode(String str, String key) throws java.io.UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

        str = str.replace('-', '+');
        str = str.replace('_', '/');
        str = str.replace('~', '=');

        byte[] textBytes = Base64.decode(str, Base64.NO_WRAP);
        AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
        SecretKeySpec newKey = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, newKey, ivSpec);
        return new String(cipher.doFinal(textBytes), "UTF-8");
    }
}