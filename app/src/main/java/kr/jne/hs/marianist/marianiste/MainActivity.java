package kr.jne.hs.marianist.marianiste;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import kr.jne.hs.marianist.marianiste.base.BaseActivity;
import kr.jne.hs.marianist.marianiste.intro.SplashActivity;
import kr.jne.hs.marianist.marianiste.service.ServiceActivity;


public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goToNext();
    }

    private void goToNext(){
        Intent intent=new Intent(this, SplashActivity.class);
        startActivity(intent);
    }
}
