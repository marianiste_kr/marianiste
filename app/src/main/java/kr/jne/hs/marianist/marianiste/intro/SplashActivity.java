package kr.jne.hs.marianist.marianiste.intro;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import kr.jne.hs.marianist.marianiste.R;
import kr.jne.hs.marianist.marianiste.base.BaseActivity;
import kr.jne.hs.marianist.marianiste.sign.SignActivity;

import static kr.jne.hs.marianist.marianiste.application.MyApplication.COMMAND;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                goToNext();
            }
        }, 2000);
    }

    private void goToNext() {
        Intent intent = new Intent(this, IntroSwipeActivity.class);
        COMMAND.clearActivityStack(intent);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
