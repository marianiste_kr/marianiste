package kr.jne.hs.marianist.marianiste.util.consts;

/**
 * Created by Penny on 2017-04-20.
 */

public class E {
    public class Key {
        public static final String STORAGE_KEY = "pref";
        public static final String SECURE_KEY = "SECURE_KEY";
    }

    public static class Net {
        public static String SECURE_KEY = "";
    }

    public static class GlobalVariable {
    }
}
