package kr.jne.hs.marianist.marianiste.util.db;

import android.content.Context;
import android.content.SharedPreferences;

import kr.jne.hs.marianist.marianiste.util.consts.E;
import kr.jne.hs.marianist.marianiste.util.enc.AES256Cipher;

/**
 * Created by sucrow on 2017-01-19.
 * sharedPreferences를 효율적으로 사용하는 방법
 * sharedPreferences는 xml에 데이터를 저장하기 때문에 입출력 속도가 느리다.
 * key와 value로 사용되기 때문에 sharedPreferences 객체를 여러번 생성할 필요가 없다.
 * 따라서 singleten으로 생성하여 관리한다.
 */


public class StorageHelper {
    private static StorageHelper ourInstance = null;
    private Context context;

    public synchronized static StorageHelper getInstance(Context context) {
        if (ourInstance == null) ourInstance = new StorageHelper(context);
        return ourInstance;
    }

    private StorageHelper(Context context) {
        this.context = context;
    }

    //String을 저장하는 method
    public void setString(String key, String value) {
        //storage 획득
        SharedPreferences.Editor editor = context.getSharedPreferences(E.Key.STORAGE_KEY, 0).edit();
        //store
        editor.putString(key, AES256Cipher.Encryption(value));
        //commit과 apply에는 차이가 있으니 확인해서 사용할 것.
        editor.commit();
    }

    public void allClear() {
        SharedPreferences.Editor editor = context.getSharedPreferences(E.Key.STORAGE_KEY, 0).edit();
        editor.clear();
        editor.commit();
    }


    //저장한 String을 불러오는 method
    public String getString(String key) {
        return AES256Cipher.Decryption(context.getSharedPreferences(E.Key.STORAGE_KEY, 0).getString(key, ""));
    }

    //Boolean을 저장하는 method
    public void setBoolean(String key, Boolean value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(E.Key.STORAGE_KEY, 0).edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    //저장한 Boolean을 불러오는 method
    public Boolean getBoolean(String key) {
        return context.getSharedPreferences(E.Key.STORAGE_KEY, 0).getBoolean(key, false);
    }

    //Int를 저장하는 method
    public void setInt(String key, int value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(E.Key.STORAGE_KEY, 0).edit();
        editor.putInt(key, value);
        editor.commit();
    }

    //저장한 int를 불러오는 method
    public int getInt(String key) {
        return context.getSharedPreferences(E.Key.STORAGE_KEY, 0).getInt(key, 0);
    }

    //Long을 저장하는 method
    public void setLong(String key, long value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(E.Key.STORAGE_KEY, 0).edit();
        editor.putLong(key, value);
        editor.commit();
    }

    //저장한 Long을 불러오는 method
    public long getLong(String key) {
        return context.getSharedPreferences(E.Key.STORAGE_KEY, 0).getLong(key, 0);
    }
}
