package kr.jne.hs.marianist.marianiste.setting;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import kr.jne.hs.marianist.marianiste.R;

public class SettingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
    }
}
