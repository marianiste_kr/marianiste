package kr.jne.hs.marianist.marianiste.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Button;

import static android.content.Context.SENSOR_SERVICE;


/**
 * Created by penny on 2017-04-20.
 */

public class Command {
    Context context;

    public Command(Context context) {
        this.context = context;
    }

    public void clearActivityStack(Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    }

    public void setColorFilter(Button button) {
//        button.getBackground().setColorFilter(new LightingColorFilter(context.getResources().getColor(R.color.colorCuteYellow), context.getResources().getColor(R.color.colorTransparent)));
//        button.setTextColor(context.getResources().getColor(R.color.blueGray50));
    }

    public float convertDpToPixel(float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }


    public String getTimeData(java.util.Calendar calendar) {
        calendar.setTimeInMillis(System.currentTimeMillis());
        String month, date;
        if ((calendar.get(calendar.MONTH) + 1) < 10)
            month = "0" + (calendar.get(calendar.MONTH) + 1);
        else
            month = ""+(calendar.get(calendar.MONTH) + 1);
        if (calendar.get(calendar.DATE) < 10)
            date = "0" + calendar.get(calendar.DATE);
        else
            date = ""+ calendar.get(calendar.DATE);
        return calendar.get(calendar.YEAR) + "" + month + "" + date;
    }

    public static String getVersionCode(Context context){
        try {
            PackageInfo packageInfo=context.getPackageManager().getPackageInfo(context.getPackageName(),0);
            return packageInfo.versionCode+"";
        } catch (PackageManager.NameNotFoundException e) {
            return  "";
        }
    }
}
