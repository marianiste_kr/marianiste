package kr.jne.hs.marianist.marianiste.sign;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import kr.jne.hs.marianist.marianiste.R;
import kr.jne.hs.marianist.marianiste.base.BaseActivity;
import kr.jne.hs.marianist.marianiste.intro.IntroSwipeActivity;
import kr.jne.hs.marianist.marianiste.service.ServiceActivity;

import static kr.jne.hs.marianist.marianiste.application.MyApplication.COMMAND;

public class SignActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
    }

    public void signBtnOnClick(View v) {
        switch (v.getId()) {
            case R.id.signUp:
                break;
            case R.id.logIn:
                Intent intent = new Intent(this, ServiceActivity.class);
                COMMAND.clearActivityStack(intent);
                startActivity(intent);
                break;
        }
    }
}
